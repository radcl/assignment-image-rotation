#include "../include/files.h"
#include "../include/reading_writing.h"
#include "../include/transformations.h"
#include <inttypes.h>
#include <malloc.h>
#include <stdio.h>

const char* const str_open_rights[] = {
    [WRITING_ONLY] = "wb",
    [READING_ONLY] = "rb",
    [READING_WRITING] = "rwb"
};

int main(int argc, char* argv[]) {
    if (argc != 3) { fprintf(stderr, "Wrong count of args."); return -1; } 
    
    struct image input;
    struct image output;
    FILE* input_file; FILE* output_file;

    /* Opening input file */
    struct maybe_file mb_input_file = open_file(argv[1], str_open_rights[READING_WRITING]);
    if (mb_input_file.status == OPEN_OK) { input_file = mb_input_file.file; }
    else {
        fprintf(stderr, "An error during input file opening.");
        return -1;
    }

    /* Reading, closing input file, rotating */
    

    if ( from_bmp(input_file, &input) != READ_DATA_OK ) { 
        free(input.data);
        close_file(input_file);
        return -1; 
    }
    close_file(input_file);

    if (!prepare_output(&input, &output)) return -1;

    if (rotate(&input, &output) != ROTATE_OK) {
        fprintf(stderr, "An error during image rotation.");
        free(input.data);
        return -1;
    }
    free(input.data);

    /* Opening output file */
    struct maybe_file mb_output_file = open_file(argv[2], str_open_rights[WRITING_ONLY]);
    if (mb_output_file.status == OPEN_OK) { output_file = mb_output_file.file; }
    else { 
        free(output.data);
        return -1;
    }

    /* Writing, closing output file */
    if ( to_bmp(output_file, &output) != WRITE_OK ) {
        close_file(output_file);
        free(output.data);
        return -1;
    }
    if ( close_file(output_file) != CLOSE_OK ) {
        fprintf(stderr, "An error during output file closing."); 
        free(output.data);
        return -1;
    }
    free(output.data);

    return 0;
}
