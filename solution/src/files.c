#include "../include/files.h"

struct maybe_file open_file(const char* filename, const char* rights) {
    FILE* file = fopen(filename, rights);
    if (file) { return (struct maybe_file) {.file = file, .status = OPEN_OK}; }
    else return (struct maybe_file) {.file = NULL, .status = OPEN_FILE_NOT_FOUND};
}

enum close_status close_file(FILE* file) {
    if (fclose(file) == EOF ) { return CLOSE_ERROR; }
    return CLOSE_OK;
}

const char* const close_status_messages[] = {
        [CLOSE_ERROR] = "Can't close file stream.\n"
};
