#include "../include/reading_writing.h"
#include <malloc.h>

bool check_header(struct bmp_header* header);

enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header input_header;
    if ( read_header(in, &input_header) != READ_HEADER_OK ) return READ_INVALID_HEADER; // reading image header
    img->width = input_header.biWidth; img->height = input_header.biHeight; // setting image size in pixels
    img->data = allocate_memory(get_image_size(img)); // allocating memory for image data
    return read_data(in, img); // reading image data
}

enum read_status read_header(FILE* in, struct bmp_header* header) {
    if ( !fread(header, sizeof(struct bmp_header), 1, in) ) return READ_INVALID_HEADER;
    return READ_HEADER_OK;
}

enum read_status read_data(FILE* in, struct image* img) {
    if (img->data == NULL) {
        printf("%s", read_status_msg[READ_MALLOC_ERROR]);
        return READ_MALLOC_ERROR;
    }

    /* Reading image data */
    if ( fread(img->data, get_image_size(img), 1, in) != 1 ) return READ_INVALID_BITS;
    return READ_DATA_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    /* Changing output header data */
    struct bmp_header output_header = create_bmp_header(img);

    /* Writing output header and image data */
    if ( write_header(out, &output_header) != WRITE_OK ) {
        fprintf(stderr, "%s", read_status_msg[WRITE_ERROR_INVALID_HEADER]);
        return WRITE_ERROR_INVALID_HEADER;
    }
    if ( write_image_data(out, img) != WRITE_OK ) {
        fprintf(stderr, "%s", write_status_msg[WRITE_ERROR_INVALID_DATA]);
        return WRITE_ERROR_INVALID_DATA;
    }

    return WRITE_OK;
}

enum write_status write_header(FILE* out, struct bmp_header* header) {
    if( fwrite(header, sizeof(struct bmp_header), 1, out) != 1 ) return WRITE_ERROR_INVALID_HEADER;
    return WRITE_OK;
}

enum write_status write_image_data(FILE* output_file, struct image const* img) {
    if ( fwrite(img->data, get_image_size(img), 1, output_file) != 1 ) return WRITE_ERROR_INVALID_DATA;
    return WRITE_OK;
}

const char* const read_status_msg[]  = {
    [READ_INVALID_HEADER] = "Header is invalid.\n",
    [READ_MALLOC_ERROR] = "An error during memory allocation.\n",
    [READ_INVALID_BITS] = "Image data is invalid.\n"
};

const char* const write_status_msg[] = {
        [WRITE_ERROR_INVALID_HEADER] = "Can't write output header.\n",
        [WRITE_ERROR_INVALID_DATA] = "Can't write output image data.\n"
};
