#include "../include/internal_formats_processing.h"
#include "../include/bmp_processing.h"
#include <stdlib.h>
#include <stdio.h>

/* Calculates image size in bytes */
size_t get_image_size(struct image const* img) {
    const uint8_t bitCount = 24; // source image bit count
    return bitCount / 8 * img->width * img->height + img->height * calculate_padding(img->width);
}

int prepare_output(struct image const* input, struct image* output) {
    output->width = input->height;
    output->height = input->width;
    output->data = allocate_memory(get_image_size(output));
    if (output->data == NULL) {
        fprintf(stderr, "An error during allocating memory for output image.");
        return 0;
    }
    return 1;
}
