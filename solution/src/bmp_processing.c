#include "../include/bmp_processing.h"
#include "../include/internal_formats_processing.h"

uint8_t calculate_padding(uint32_t const image_width) {
    return (uint8_t) (4 - ((image_width * 3) % 4)) % 4;
}

struct bmp_header create_bmp_header(struct image const* img) {
    struct bmp_header output_header = {0};

    const uint32_t HEADER_SIZE = sizeof(struct bmp_header);
    const uint32_t INFO_HEADER_SIZE = 40;
    const uint16_t BMP_IDENTIFIER = 0x4D42;
    const uint16_t PLANES_NUM = 1;
    const uint16_t BIT_COUNT = 24;


    output_header.bfType = BMP_IDENTIFIER;
    output_header.biPlanes = PLANES_NUM;
    output_header.biSize = INFO_HEADER_SIZE;
    output_header.bfileSize = HEADER_SIZE + get_image_size(img);
    output_header.biSizeImage = get_image_size(img);
    output_header.biWidth = img->width;
    output_header.biHeight = img->height;
    output_header.biBitCount = BIT_COUNT;
    output_header.bOffBits = HEADER_SIZE;

    return output_header;
}

struct pixel* allocate_memory(size_t query) {
    return malloc(query);
}
