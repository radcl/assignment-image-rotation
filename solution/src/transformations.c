#include "../include/transformations.h"
#include "../include/bmp_processing.h"
#include <inttypes.h>
#include <malloc.h>

enum rotate_status rotate(struct image const* source, struct image* output) {
    uint8_t padding_original = calculate_padding(source->width);
    uint8_t padding_rotated = calculate_padding(output->width);

    /* Rotating cycle */
    for (size_t row = 0; row < source->height; row++) {
        for (size_t col = 0; col < source->width; col++) {
            uint8_t* o_num = &source->data->b + 3 * (col + source->width * row) + padding_original * row; // calculating pos in original img
            uint8_t* r_num = &output->data->b + 3 * (source->height - row - 1 + source->height * col) + padding_rotated * col; // calculating pos in new img
            r_num[0] = o_num[0]; r_num[1] = o_num[1]; r_num[2] = o_num[2];
        }
    }

    return ROTATE_OK;
}
