#ifndef READING_WRITING_H
#define READING_WRITING_H
#include "bmp_processing.h"
#include "internal_formats_processing.h"
#include <stdbool.h>
#include <stdio.h>

/*  deserializer   */
enum read_status {
    READ_HEADER_OK,
    READ_DATA_OK,
    READ_MALLOC_ERROR,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

extern const char* const read_status_msg[];

enum read_status from_bmp ( FILE* in, struct image* img );
enum read_status read_header( FILE* in, struct bmp_header* header );
enum read_status read_data( FILE* in, struct image* input_img );


/*  serializer   */
enum write_status  {
    WRITE_OK,
    WRITE_ERROR_INVALID_HEADER,
    WRITE_ERROR_INVALID_DATA
};

extern const char* const write_status_msg[];

enum write_status to_bmp( FILE* out, struct image const* output_img );
enum write_status write_header( FILE* out, struct bmp_header* header);
enum write_status write_image_data(FILE* output_file, struct image const* img);

#endif
