#ifndef BMP_PROCESSING_H
#define BMP_PROCESSING_H
#include "../include/internal_formats_processing.h"
#include <inttypes.h>

/* Bmp header struct */
#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

uint8_t calculate_padding(uint32_t const image_width);

struct bmp_header create_bmp_header(struct image const* img);

struct pixel* allocate_memory(size_t query);

#endif
