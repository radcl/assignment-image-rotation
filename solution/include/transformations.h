#ifndef TRANSFORMATIONS_H
#define TRANSFORMATIONS_H
#include "internal_formats_processing.h"
#include <stdio.h>

enum rotate_status {
    ROTATE_OK,
    ROTATE_ERROR_INVALID_OUTPUT,
    ROTATE_ERROR_INVALID_DATA
};

enum rotate_status rotate(struct image const* source, struct image* output);

#endif
