#ifndef FILES_H
#define FILES_H
#include <stdio.h>

/* Opening file */
enum open_rights {
    WRITING_ONLY,
    READING_ONLY,
    READING_WRITING
};

extern const char* const str_open_rights[];

enum open_status {
    OPEN_OK,
    OPEN_FILE_NOT_FOUND
};

struct maybe_file {
    FILE* file;
    enum open_status status;
};

struct maybe_file open_file(const char* filename, const char* rights);

/* Closing file */
enum close_status {
    CLOSE_OK,
    CLOSE_ERROR
};

extern const char* const close_status_messages[];

enum close_status close_file(FILE* file);


#endif
