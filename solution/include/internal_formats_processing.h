#ifndef STRUCTURES_H
#define STRUCTURES_H
#include <stdlib.h>
#include <stdint.h>

/* Internal format structure */
struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct pixel {
    uint8_t b;
    uint8_t g;
    uint8_t r;
};

size_t get_image_size(struct image const* img);
int prepare_output(struct image const* input, struct image* output);

#endif
